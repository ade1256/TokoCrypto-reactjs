import React from "react";
import {
    Link,
    Redirect
} from "react-router-dom";
import logo from '../assets/img/logo.svg';

export const Auth = {
    isAuthenticated: localStorage.getItem("token") ? true : false,
    authenticate(cb) {
        var emailLogin = document.getElementById('login-email').value;
        var passwordLogin = document.getElementById('login-password').value;
        fetch("https://reqres.in/api/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: emailLogin,
                password: passwordLogin
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (!result.error) {
                        localStorage.setItem('token', result.token);
                        localStorage.setItem('user', JSON.stringify(result));
                        window.location = "/toko";
                    } else {
                        alert(result.error);
                    }
                },
                (error) => {
                    alert(error);
                }
            );
    },
    signout(cb) {
        console.log('ini');
        localStorage.removeItem('token');
        window.location = "/login";
    }
};

class Login extends React.Component {
    state = {
        redirectToReferrer: Auth.isAuthenticated
    };

    login = () => {
        Auth.authenticate(() => {
            if (this.isAuthenticated) {
                this.setState({ redirectToReferrer: true });
            }
        });
    };

    render() {
        const { from } = this.props.location.state || { from: { pathname: "/toko" } };
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) {
            return <Redirect to={from} />;
        }

        return (

            <div id="login">
                <div className="logo">
                <img src={logo} />
                <h1><span className="text-blue">Toko</span><span className="text-gray">Crypto</span></h1>
                </div>
                <div className="form-input">
                    <input type="email" placeholder="Email" id="login-email" />
                </div>
                <div className="form-input">
                    <input type="password" placeholder="Password" id="login-password" />
                </div>
                <div className="form-input">
                    <button type="button" className="button bg-blue" onClick={this.login}>LOGIN</button>
                </div>
                <p>*isi username dan password terserah untuk demo.</p>
            </div>
        );
    }
}

export default Login;