import React from 'react';
import { Link } from "react-router-dom";
import { Auth } from './Login';
import logo from '../assets/img/logo.svg';

class Navbar extends React.Component {
	 constructor() {
        super()
        this.state = {
            saldo: new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(localStorage.getItem('saldo')),
            myCrypto: localStorage.getItem('MyCrypto') ? JSON.parse(localStorage.getItem('MyCrypto')) : []
        }
    }
    render() {
        return (
	    <div id="top-bar">
	        <div class="logo">
	            <Link to="/toko"><img src={logo} /></Link>
	        </div>
	        <div class="search">
	            <span class="icon icon-search"></span>
	            <input type="text" name="search" class="form-search" placeholder="Search cryptocurrencies..."/>
	        </div>
	        <div class="inventory">
	           <Link to="/cryptosaya" class="text-gray">My Inventory</Link>
	        </div>
	        <div class="sell-buy">
	           <Link to="/toko" class="text-gray">Toko</Link>
	        </div>
	        <div class="menu-right">
	            <div class="money text-gray">
	                <span  class="icon icon-wallet"></span>
	                <div class="idr" id="idr">{this.state.saldo}</div>
	            </div>
	            <div class="profile text-black">
	                <div class="img-profile"><span>A</span></div>
	                <div class="name"><a href="#">Ade Prasetyo <span class="icon icon-down"></span></a></div>
	            </div>
	            <div class="logout"><a style={{float: 'right'}} onClick={Auth.signout} class="text-black" >Log Out</a></div>
	        </div>
	    </div>
        )
    }
}

export default Navbar;